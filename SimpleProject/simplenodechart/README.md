## Create and start Helm Chart

### Create Helm chart

````
helm create simplenodechart
````

### Install helm chart

````
helm install simplenodechart simplenodechart/ --values simplenodechart/values.yaml

````

### Add server details

````
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services simplenodechart)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT
````

### Curl to verify the application

````
curl http://$NODE_IP:$NODE_PORT
````

### Verify helm chart

````
helm list
````

### Delete Helm Chart

````
helm delete simplenodechart
````

### Upgrade Helm chart

````
helm upgrade simplenodechart simplenodechart/ --values simplenodechart/values.yaml
````