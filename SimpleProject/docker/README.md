## Create and Run Docker image

### Create a docker image
```
docker build -t simplenodeimage .
```

### Run Docker image in detached (-d) mode, port 9080 is mapped to the container port 8087 where node application is running
```
docker run -d -p 9080:8087 --name simplenodecontainer simplenodeimage
```

### Verify the container is running
```
docker ps
```

### Verify the node application is running and responsive
```
curl localhost:9080
```

### Stop the docker container
```
docker stop simplenodecontainer
```

### Remove the docker container
```
docker rm simplenodecontainer
```

