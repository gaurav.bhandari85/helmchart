/*var http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    var full_address = req.protocol + "://" + req.headers.host + req.originalUrl;
	res.write('Hello User\n' + full_address);
	
	
    res.end();
}).listen(8087);
*/

var http = require('http');
var os = require("os");
var hostname = os.hostname();


http.createServer(function (req, res) {
    
	res.write('\n');
	res.write('****************************************\n');
	res.write('****************************************\n');
	res.write('**** Hello User ****\n');
	res.write('Header: ' + JSON.stringify(req.headers));
	res.write('\n');
	res.write('Host: ' + req.headers.host);
	res.write('\n');
	res.write('Machine Hostname: ' + hostname);
	res.write('\n');
	res.write('Method: ' + JSON.stringify(req.method));
	res.write('\n');
	res.write('URL: ' + JSON.stringify(req.url));
	res.write('\n');
	
	res.write('****************************************\n');
	res.write('****************************************\n');
	
	
	
	
    res.end();
}).listen(8087);
