## High Level Notes
- This is a simple project which starts with Docker followed by K8s to Helm
- Docker folder 
  - A very simple and unrealistic, but workable, nodejs application
  - Dockerfile helps  to create Docker image (Refer Readme of the docker folder for more details) 
- k8s folder
  - Contains Kubernetes yaml files for installing Kubernetes kinds like deployment/ service/ INgress
  - The k8s pod uses the image generated using the dockerfile from docker folder
-simplenodechart 
   - Contains helm chart that creates the same resources as the one created by K8s folder
