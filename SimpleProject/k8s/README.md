## Crete and start pod

### Import the image from docker to microk8s

````
docker save simplenodeimage > simplenodeimage.tar
microk8s ctr image import simplenodeimage.tar
````

### Start a deployment
````
kubectl apply -f deployment.yml
````
### Verify the pod is running

````
kubectl get deployment
kubectl get pods
````

### Get the pod IP

````
kubectl get pods -o wide
````

### Curl the internal IP of k8s pod with port 8087, where our node application should be running

````
curl x.x.x.x:8087
````

### Start k8s service

````
kubectl apply -f service.yml
````

### Verify the service is running

````
kubectl get service
````

### Get the k8s node Internal IP

````
kubectl get nodes -o wide
````

### Curl the application using the Node Internal IP, Please note our service port (30000) is mapped to deployment port (8087) 

````
curl x.x.x.x:30000
````

### Remove Service

````
kubectl remove -f service.yml
````

### Remove Deployment

````
kubectl remove -f deployment.yml
````


